using UnityEngine;

public class MirrorEnabler : MonoBehaviour
{
    // Reference to the VRMirror script
    public VRMirror vrMirror;

    // When we enter on a collider
    public void OnTriggerEnter(Collider collider)
    {
        // in base of the tag of the collider gameObject
        if (collider.gameObject.tag == "Hands" && !vrMirror.mirroring)
        {
            // in case of the left hand
            if (collider.gameObject.GetComponent<OVRHand>().HandType == OVRHand.Hand.HandLeft)
                // we set to true the boolean for activate the right hand
                vrMirror.mirrorLeft = true;
                // in case of the right hand
            else if (collider.gameObject.GetComponent<OVRHand>().HandType == OVRHand.Hand.HandRight)
                // we set to true the boolean for activate the left hand
                vrMirror.mirrorRight = true;
        }

        // and in the hand we enable the mirror from the VRMirror script
        vrMirror.SetCurrentTransform();
        vrMirror.EnableMirror();
    }
}